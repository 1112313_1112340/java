/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import entity.ChiTietCuaHang;
import entity.ChiTietCuaHangId;
import entity.ChiTietHoaDon;
import entity.CuaHang;
import entity.HoaDon;
import entity.KhachHang;
import entity.NhanVien;
import java.awt.Dialog;
import java.awt.Window;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import util.HibernateUtil1;
import util.MyStringUtil;

/**
 *
 * @author Trieu Doan
 */
public class JPanelHoaDon extends javax.swing.JPanel {

    private List<CuaHang> dsCuaHang;
    private HoaDon hoaDon;
    private List<ChiTietHoaDon> dsChiTietHoaDon;
    private DefaultTableModel model;

    /**
     * Creates new form JPanelHoaDon
     */
    public JPanelHoaDon() {
        initComponents();
        taoHoaDon();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableMatHang = new javax.swing.JTable();
        jLabel6 = new javax.swing.JLabel();
        jButtonTao = new javax.swing.JButton();
        jButtonHuy = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jTextNgayBan = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jComboBoxCuaHang = new javax.swing.JComboBox();
        jComboBoxNhanVien = new javax.swing.JComboBox();
        jComboBoxKhachHang = new javax.swing.JComboBox();
        jLabel8 = new javax.swing.JLabel();
        jTextMaKhachHang = new javax.swing.JTextField();
        jTextTongTien = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jButtonThemMatHang = new javax.swing.JButton();

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel4.setText("Danh sách các mặt hàng cần nhập");

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 0, 102));
        jLabel5.setText("HÓA ĐƠN");

        jTableMatHang.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "Mặt hàng", "Đơn giá gốc", "Số lượng", "Khuyến mãi", "Thành tiền"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.String.class, java.lang.Integer.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jTableMatHang.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableMatHangMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTableMatHang);

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel6.setText("Khách hàng");

        jButtonTao.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jButtonTao.setText("Tạo hóa đơn");
        jButtonTao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonTaoActionPerformed(evt);
            }
        });

        jButtonHuy.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jButtonHuy.setText("Hủy");
        jButtonHuy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonHuyActionPerformed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(0, 51, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/icon.png"))); // NOI18N
        jLabel9.setText("jLabel9");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(24, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, 96, Short.MAX_VALUE)
                .addGap(23, 23, 23))
        );

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        jLabel11.setText("Family Mart");

        jPanel2.setBackground(new java.awt.Color(204, 255, 204));

        jTextNgayBan.setEditable(false);
        jTextNgayBan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextNgayBanActionPerformed(evt);
            }
        });

        jLabel7.setText("Ngày bán");

        jLabel3.setText("Nhân viên bán");

        jLabel1.setText("Cửa hàng");

        jComboBoxCuaHang.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBoxCuaHangItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jComboBoxNhanVien, 0, 230, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextNgayBan))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jComboBoxCuaHang, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(jTextNgayBan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jComboBoxCuaHang, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jComboBoxNhanVien, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(34, Short.MAX_VALUE))
        );

        jComboBoxKhachHang.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jComboBoxKhachHang.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Khách hàng vãng lai", "Khách hàng thân thiết" }));
        jComboBoxKhachHang.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBoxKhachHangItemStateChanged(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel8.setText("Mã khách hàng");

        jTextMaKhachHang.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jTextTongTien.setEditable(false);
        jTextTongTien.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jTextTongTien.setForeground(new java.awt.Color(255, 51, 51));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 51, 51));
        jLabel2.setText("Tổng cộng");

        jButtonThemMatHang.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButtonThemMatHang.setText("Thêm mặt hàng");
        jButtonThemMatHang.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonThemMatHangActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jButtonHuy, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jButtonTao, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addComponent(jLabel2)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jTextTongTien, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(19, 19, 19)
                                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(327, 327, 327)
                                .addComponent(jLabel5))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(56, 56, 56)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel8)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel11)
                                        .addGap(211, 211, 211)
                                        .addComponent(jLabel6)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jComboBoxKhachHang, 0, 195, Short.MAX_VALUE)
                                    .addComponent(jTextMaKhachHang))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButtonThemMatHang, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(56, 56, 56)
                        .addComponent(jLabel5))
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(jLabel6)
                    .addComponent(jComboBoxKhachHang, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(jTextMaKhachHang, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26)
                .addComponent(jButtonThemMatHang, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextTongTien, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 63, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButtonTao, javax.swing.GroupLayout.DEFAULT_SIZE, 46, Short.MAX_VALUE)
                    .addComponent(jButtonHuy, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(23, 23, 23))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonTaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonTaoActionPerformed
        int loaiKH = this.jComboBoxKhachHang.getSelectedIndex();
        KhachHang khachHang = null;
        if (loaiKH > 0) {
            khachHang = layKhachHang();
            if (khachHang == null) {
                JOptionPane.showMessageDialog(this, "Sai mã khách hàng!");
            }
        }
        if (loaiKH == 0 || khachHang != null) {
            CuaHang cuaHang = dsCuaHang.get(this.jComboBoxCuaHang.getSelectedIndex());
            NhanVien nhanVien = (NhanVien) cuaHang.getNhanViens().toArray()[this.jComboBoxNhanVien.getSelectedIndex()];
            hoaDon.setCuaHang(cuaHang);
            hoaDon.setNhanVien(nhanVien);
            int soLuong = dsChiTietHoaDon.size();
            if (soLuong > 0) {
                try {
                    hoaDon.setSoLuong(soLuong);
                    int tongTien = Integer.parseInt(this.jTextTongTien.getText());
                    hoaDon.setThanhTien(tongTien);
                    hoaDon.setThue(0);
                    hoaDon.setTongCong(tongTien);
                    Session session = HibernateUtil1.getSessionFactory().openSession();
                    session.beginTransaction();
                    session.save(hoaDon);
                    hoaDon.setMaNhanDang("N" + hoaDon.getMaSo());
                    session.update(hoaDon);
                    for (ChiTietHoaDon c : dsChiTietHoaDon) {
                        c.getId().setHoaDon(hoaDon.getMaSo());
                        session.save(c);
                    }
                    session.getTransaction().commit();
                } catch (HibernateException ex) {
                    JOptionPane.showMessageDialog(this, ex.getMessage());
                }
                capNhatCuaHang();
                Window w = SwingUtilities.getWindowAncestor(this);
                w.dispose();
            } else {
                JOptionPane.showMessageDialog(this, "Chưa có mặt hàng nào trong hóa đơn!");
            }
        }
    }//GEN-LAST:event_jButtonTaoActionPerformed

    private void jButtonHuyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonHuyActionPerformed
        Window w = SwingUtilities.getWindowAncestor(this);
        w.dispose();
    }//GEN-LAST:event_jButtonHuyActionPerformed

    private void jTextNgayBanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextNgayBanActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextNgayBanActionPerformed

    private void jComboBoxCuaHangItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBoxCuaHangItemStateChanged
        this.jComboBoxNhanVien.removeAllItems();
        CuaHang c = this.dsCuaHang.get(this.jComboBoxCuaHang.getSelectedIndex());
        for (Object v : c.getNhanViens()) {
            this.jComboBoxNhanVien.addItem(((NhanVien) v).getHoten());
        }
    }//GEN-LAST:event_jComboBoxCuaHangItemStateChanged

    private void jButtonThemMatHangActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonThemMatHangActionPerformed
        JPanelHoaDon_ThemMatHang jPanel = new JPanelHoaDon_ThemMatHang(this);
        Dialog dialog = new Dialog((Dialog) this.getParent(), "Hóa đơn - Thêm mặt hàng", true);
        dialog.add(jPanel);
        dialog.pack();
        dialog.setVisible(true);
    }//GEN-LAST:event_jButtonThemMatHangActionPerformed

    private void jTableMatHangMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableMatHangMouseClicked
        JPanelHoaDon_CapNhatMatHang jPanel = new JPanelHoaDon_CapNhatMatHang(this);
        Dialog dialog = new Dialog((Dialog) this.getParent(), "Hóa đơn - Cập nhật mặt hàng", true);
        dialog.add(jPanel);
        dialog.pack();
        dialog.setVisible(true);
    }//GEN-LAST:event_jTableMatHangMouseClicked

    private void jComboBoxKhachHangItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBoxKhachHangItemStateChanged
        if (this.jComboBoxKhachHang.getSelectedIndex() == 0) {
            this.jTextMaKhachHang.setEnabled(false);
        } else {
            this.jTextMaKhachHang.setEnabled(true);
        }
    }//GEN-LAST:event_jComboBoxKhachHangItemStateChanged


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonHuy;
    private javax.swing.JButton jButtonTao;
    private javax.swing.JButton jButtonThemMatHang;
    private javax.swing.JComboBox jComboBoxCuaHang;
    private javax.swing.JComboBox jComboBoxKhachHang;
    private javax.swing.JComboBox jComboBoxNhanVien;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTableMatHang;
    private javax.swing.JTextField jTextMaKhachHang;
    private javax.swing.JTextField jTextNgayBan;
    private javax.swing.JTextField jTextTongTien;
    // End of variables declaration//GEN-END:variables

    private void taoHoaDon() {
        hoaDon = new HoaDon();
        dsChiTietHoaDon = new ArrayList<>();
        model = (DefaultTableModel) this.jTableMatHang.getModel();
        model.setNumRows(0);
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        this.jTextNgayBan.setText(dateFormat.format(date));
        hoaDon.setNgayBan(date);
        docDanhSachCuaHang();
    }

    private void docDanhSachCuaHang() {
        try {
            Session session = HibernateUtil1.getSessionFactory().openSession();
            session.beginTransaction();
            dsCuaHang = session.createQuery("from CuaHang").list();
            session.getTransaction().commit();
            for (CuaHang c : dsCuaHang) {
                this.jComboBoxCuaHang.addItem(c.getTen());
            }
        } catch (HibernateException ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage());
        }
    }

    void themMatHang(ChiTietHoaDon chiTietHoaDon) {
        this.dsChiTietHoaDon.add(chiTietHoaDon);
        Object[] oneRow = new Object[5];
        oneRow[0] = chiTietHoaDon.getMatHang().getTen();
        oneRow[1] = chiTietHoaDon.getDonGiaGoc();
        oneRow[2] = chiTietHoaDon.getSoLuong();
        int thanhTien = chiTietHoaDon.getDonGiaGoc() * chiTietHoaDon.getSoLuong();
        if (chiTietHoaDon.getKhuyenMai() != null) {
            oneRow[3] = chiTietHoaDon.getKhuyenMai().getThongTin();
            thanhTien -= chiTietHoaDon.getGiamGia();
        }
        oneRow[4] = thanhTien;
        model.addRow(oneRow);
        int tongTien = tinhTongTien();
        this.jTextTongTien.setText(String.valueOf(tongTien));
    }

    void capNhatMatHangDuocChon() {
        int index = this.jTableMatHang.getSelectedRow();
        ChiTietHoaDon c = this.dsChiTietHoaDon.get(index);

        model.setValueAt(c.getMatHang().getTen(), index, 0);
        model.setValueAt(c.getDonGiaGoc(), index, 1);
        model.setValueAt(c.getSoLuong(), index, 2);
        int thanhTien = c.getDonGiaGoc() * c.getSoLuong();

        if (c.getKhuyenMai() != null) {
            model.setValueAt(c.getKhuyenMai().getThongTin(), index, 3);
            thanhTien -= c.getGiamGia();
        }
        model.setValueAt(thanhTien, index, 4);
        int tongTien = tinhTongTien();
        this.jTextTongTien.setText(String.valueOf(tongTien));
    }

    void xoaMatHangDuocChon() {
        int index = this.jTableMatHang.getSelectedRow();
        this.dsChiTietHoaDon.remove(index);
        model.removeRow(index);
    }

    private int tinhTongTien() {
        int tong = 0;
        for (ChiTietHoaDon c : dsChiTietHoaDon) {
            tong += c.getDonGiaGoc() * c.getSoLuong() - c.getGiamGia();
        }

        return tong;
    }

    private KhachHang layKhachHang() {
        KhachHang k = null;
        String str = this.jTextMaKhachHang.getText();
        if (MyStringUtil.checkNumberString(str)) {
            int MaKH = Integer.parseInt(str);
            try {
                Session session = HibernateUtil1.getSessionFactory().openSession();
                session.beginTransaction();
                k = (KhachHang) session.get(KhachHang.class, MaKH);
                session.getTransaction().commit();
            } catch (HibernateException ex) {
                JOptionPane.showMessageDialog(this, "Nhập sai mã khách hàng!");
            }

        }
        return k;
    }

    private void capNhatCuaHang() {
        for (ChiTietHoaDon c : dsChiTietHoaDon) {
            try {
                Session session = HibernateUtil1.getSessionFactory().openSession();
                session.beginTransaction();
                int maCuaHang = layCuaHang().getMaSo();
                ChiTietCuaHangId id = new ChiTietCuaHangId(maCuaHang, c.getMatHang().getMaSo());
                ChiTietCuaHang ctch = (ChiTietCuaHang) session.get(ChiTietCuaHang.class, id);
                ctch.setSoLuong(ctch.getSoLuong() - c.getSoLuong());
                session.update(ctch);
                session.getTransaction().commit();
            } catch (HibernateException ex) {
                JOptionPane.showMessageDialog(this, ex.getMessage());
            }
        }
    }

    CuaHang layCuaHang() {
        return this.dsCuaHang.get(this.jComboBoxCuaHang.getSelectedIndex());
    }

    ChiTietHoaDon layChiTietHoaDon() {
        return this.dsChiTietHoaDon.get(this.jTableMatHang.getSelectedRow());
    }
}
