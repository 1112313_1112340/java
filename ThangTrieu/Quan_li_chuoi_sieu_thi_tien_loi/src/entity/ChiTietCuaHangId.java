package entity;
// Generated Jun 15, 2014 4:10:49 AM by Hibernate Tools 3.6.0



/**
 * ChiTietCuaHangId generated by hbm2java
 */
public class ChiTietCuaHangId  implements java.io.Serializable {


     private int cuaHang;
     private int matHang;

    public ChiTietCuaHangId() {
    }

    public ChiTietCuaHangId(int cuaHang, int matHang) {
       this.cuaHang = cuaHang;
       this.matHang = matHang;
    }
   
    public int getCuaHang() {
        return this.cuaHang;
    }
    
    public void setCuaHang(int cuaHang) {
        this.cuaHang = cuaHang;
    }
    public int getMatHang() {
        return this.matHang;
    }
    
    public void setMatHang(int matHang) {
        this.matHang = matHang;
    }


   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof ChiTietCuaHangId) ) return false;
		 ChiTietCuaHangId castOther = ( ChiTietCuaHangId ) other; 
         
		 return (this.getCuaHang()==castOther.getCuaHang())
 && (this.getMatHang()==castOther.getMatHang());
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + this.getCuaHang();
         result = 37 * result + this.getMatHang();
         return result;
   }   


}


