/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package util;

/**
 *
 * @author Trieu Doan
 */
public class MyStringUtil {

    public static boolean checkNumberString(String text) {
        if(!text.isEmpty()){
            int i, n;
            n = text.length();
            for(i = 0; i < n; i++){
                if(text.charAt(i) < '0' || text.charAt(i) > '9')
                    return false;
            }
            return true;
        }
        return false;
    }
    
}
