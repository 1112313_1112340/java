CREATE DATABASE  IF NOT EXISTS `familymartdb` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `familymartdb`;
-- MySQL dump 10.13  Distrib 5.6.13, for Win32 (x86)
--
-- Host: localhost    Database: familymartdb
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `chi_tiet_hoa_don`
--

DROP TABLE IF EXISTS `chi_tiet_hoa_don`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chi_tiet_hoa_don` (
  `HOA_DON` int(11) NOT NULL DEFAULT '0',
  `MAT_HANG` int(11) NOT NULL DEFAULT '0',
  `KHUYEN_MAI` int(11) DEFAULT NULL,
  `SO_LUONG` int(11) DEFAULT NULL,
  `DON_GIA_GOC` int(11) DEFAULT NULL,
  `GIAM_GIA` int(11) DEFAULT NULL,
  PRIMARY KEY (`HOA_DON`,`MAT_HANG`),
  KEY `MAT_HANG_CTHD_FKEY` (`MAT_HANG`),
  KEY `KHUYEN_MAI_CTHD_FKEY` (`KHUYEN_MAI`),
  CONSTRAINT `HOA_DON_CTHD_FKEY` FOREIGN KEY (`HOA_DON`) REFERENCES `hoa_don` (`MA_SO`),
  CONSTRAINT `KHUYEN_MAI_CTHD_FKEY` FOREIGN KEY (`KHUYEN_MAI`) REFERENCES `khuyen_mai` (`MA_SO`),
  CONSTRAINT `MAT_HANG_CTHD_FKEY` FOREIGN KEY (`MAT_HANG`) REFERENCES `mat_hang` (`MA_SO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chi_tiet_hoa_don`
--

LOCK TABLES `chi_tiet_hoa_don` WRITE;
/*!40000 ALTER TABLE `chi_tiet_hoa_don` DISABLE KEYS */;
/*!40000 ALTER TABLE `chi_tiet_hoa_don` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chi_tiet_kho_hang`
--

DROP TABLE IF EXISTS `chi_tiet_kho_hang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chi_tiet_kho_hang` (
  `KHO_HANG` int(11) NOT NULL DEFAULT '0',
  `MAT_HANG` int(11) NOT NULL DEFAULT '0',
  `SO_LUONG` int(11) DEFAULT NULL,
  `MIN` int(11) DEFAULT NULL,
  `MAX` int(11) DEFAULT NULL,
  PRIMARY KEY (`KHO_HANG`,`MAT_HANG`),
  KEY `MAT_HANG_CTKH_FKEY` (`MAT_HANG`),
  CONSTRAINT `KHO_HANG_CTKH_FKEY` FOREIGN KEY (`KHO_HANG`) REFERENCES `kho_hang` (`MA_SO`),
  CONSTRAINT `MAT_HANG_CTKH_FKEY` FOREIGN KEY (`MAT_HANG`) REFERENCES `mat_hang` (`MA_SO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chi_tiet_kho_hang`
--

LOCK TABLES `chi_tiet_kho_hang` WRITE;
/*!40000 ALTER TABLE `chi_tiet_kho_hang` DISABLE KEYS */;
INSERT INTO `chi_tiet_kho_hang` VALUES (1,2,NULL,1000,10),(1,3,NULL,100,10),(1,4,NULL,1000,20),(1,5,NULL,100,2),(1,6,NULL,1000,5),(1,7,NULL,2000,10);
/*!40000 ALTER TABLE `chi_tiet_kho_hang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chi_tiet_phieu_nhap`
--

DROP TABLE IF EXISTS `chi_tiet_phieu_nhap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chi_tiet_phieu_nhap` (
  `PHIEU_NHAP_HANG` int(11) NOT NULL DEFAULT '0',
  `MAT_HANG` int(11) NOT NULL DEFAULT '0',
  `SO_LUONG` int(11) DEFAULT NULL,
  `DON_GIA` int(11) DEFAULT NULL,
  PRIMARY KEY (`PHIEU_NHAP_HANG`,`MAT_HANG`),
  KEY `MAT_HANG_CTPN_FKEY` (`MAT_HANG`),
  CONSTRAINT `MAT_HANG_CTPN_FKEY` FOREIGN KEY (`MAT_HANG`) REFERENCES `mat_hang` (`MA_SO`),
  CONSTRAINT `PHIEU_NHAP_HANG_CTPN_FKEY` FOREIGN KEY (`PHIEU_NHAP_HANG`) REFERENCES `phieu_nhap_hang` (`MA_SO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chi_tiet_phieu_nhap`
--

LOCK TABLES `chi_tiet_phieu_nhap` WRITE;
/*!40000 ALTER TABLE `chi_tiet_phieu_nhap` DISABLE KEYS */;
/*!40000 ALTER TABLE `chi_tiet_phieu_nhap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chi_tiet_phieu_xuat`
--

DROP TABLE IF EXISTS `chi_tiet_phieu_xuat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chi_tiet_phieu_xuat` (
  `PHIEU_XUAT_HANG` int(11) NOT NULL DEFAULT '0',
  `MAT_HANG` int(11) NOT NULL DEFAULT '0',
  `SO_LUONG` int(11) DEFAULT NULL,
  `DON_GIA` int(11) DEFAULT NULL,
  PRIMARY KEY (`PHIEU_XUAT_HANG`,`MAT_HANG`),
  KEY `MAT_HANG_CTPX_FKEY` (`MAT_HANG`),
  CONSTRAINT `MAT_HANG_CTPX_FKEY` FOREIGN KEY (`MAT_HANG`) REFERENCES `mat_hang` (`MA_SO`),
  CONSTRAINT `PHIEU_XUAT_HANG_CTPX_FKEY` FOREIGN KEY (`PHIEU_XUAT_HANG`) REFERENCES `phieu_xuat_hang` (`MA_SO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chi_tiet_phieu_xuat`
--

LOCK TABLES `chi_tiet_phieu_xuat` WRITE;
/*!40000 ALTER TABLE `chi_tiet_phieu_xuat` DISABLE KEYS */;
/*!40000 ALTER TABLE `chi_tiet_phieu_xuat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cua_hang`
--

DROP TABLE IF EXISTS `cua_hang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cua_hang` (
  `MA_SO` int(11) NOT NULL,
  `TEN` text,
  `DIA_CHI` text,
  `DIEN_THOAI` text,
  PRIMARY KEY (`MA_SO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cua_hang`
--

LOCK TABLES `cua_hang` WRITE;
/*!40000 ALTER TABLE `cua_hang` DISABLE KEYS */;
INSERT INTO `cua_hang` VALUES (1,'Cửa hàng Saigon Sky Garden','20 Lê Thánh Tôn, Phường Bến Nghé, Quận 1','3823-4796'),(2,'Cửa hàng ZENPLAZA','Hầm B1, ZENPLAZA, 54-56 Nguyễn Trãi, Phường Bến Thành, Quận 1','3925-5096'),(3,'Cửa hàng 126 Lý Tự Trọng','126 Lý Tự Trọng, Phường Bến Thành, Quận 1','3823-9604'),(4,'Cửa hàng 134 Pasteur','134 Pasteur, Phường Bến Nghé, Quận 1','3827-4396'),(5,'Cửa hàng 58 Pasteur','58 Pasteur, Phường Bến Nghé, Quận 1','3914-7538'),(6,'Cửa hàng Lê Thánh Tôn, chợ Bến Thành','189 Lê Thánh Tôn, Phường Bến Thành, Quận 1','3822-4524'),(7,'Cửa hàng 137 Cô Bắc','137-139 Cô Bắc, Phường Cô Giang, Quận 1','3838-9264'),(8,'Cửa hàng 66C Phó Đức Chính','66C Phó Đức Chính, Phường Nguyễn Thái Bình, Quận 1','3914-6075'),(9,'Cửa hàng 97-99 Nguyễn Hữu Cầu','97-99 Nguyễn Hữu Cầu, Phường Tân Định, Quận 1',' 3820-3950'),(10,'Cửa hàng 234 Đề Thám','234 Đề Thám, Phường Phạm Ngũ Lão, Quận 1','3920-6214');
/*!40000 ALTER TABLE `cua_hang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hoa_don`
--

DROP TABLE IF EXISTS `hoa_don`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hoa_don` (
  `MA_SO` int(11) NOT NULL,
  `MA_NHAN_DANG` text,
  `NGAY_BAN` date DEFAULT NULL,
  `NHAN_VIEN_BAN` int(11) DEFAULT NULL,
  `CUA_HANG` int(11) DEFAULT NULL,
  `KHACH_HANG` int(11) DEFAULT NULL,
  `SO_LUONG` int(11) DEFAULT NULL,
  `THANH_TIEN` int(11) DEFAULT NULL,
  `THUE` int(11) DEFAULT NULL,
  `TONG_CONG` int(11) DEFAULT NULL,
  PRIMARY KEY (`MA_SO`),
  KEY `NHAN_VIEN_BAN_HOADON_FKEY` (`NHAN_VIEN_BAN`),
  KEY `CUA_HANG_HOADON_FKEY` (`CUA_HANG`),
  KEY `KHACH_HANG_HOADON_FKEY` (`KHACH_HANG`),
  CONSTRAINT `CUA_HANG_HOADON_FKEY` FOREIGN KEY (`CUA_HANG`) REFERENCES `cua_hang` (`MA_SO`),
  CONSTRAINT `KHACH_HANG_HOADON_FKEY` FOREIGN KEY (`KHACH_HANG`) REFERENCES `khach_hang` (`MA_SO`),
  CONSTRAINT `NHAN_VIEN_BAN_HOADON_FKEY` FOREIGN KEY (`NHAN_VIEN_BAN`) REFERENCES `nhan_vien` (`MA_SO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hoa_don`
--

LOCK TABLES `hoa_don` WRITE;
/*!40000 ALTER TABLE `hoa_don` DISABLE KEYS */;
/*!40000 ALTER TABLE `hoa_don` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `khach_hang`
--

DROP TABLE IF EXISTS `khach_hang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `khach_hang` (
  `MA_SO` int(11) NOT NULL,
  `HO_TEN` int(11) DEFAULT NULL,
  `DIA_CHI` text,
  `DIEN_THOAI` text,
  `LA_KHACH_HANG_THAN_THIET` tinyint(1) DEFAULT NULL,
  `DIEM_TICH_LUY` int(11) DEFAULT NULL,
  PRIMARY KEY (`MA_SO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `khach_hang`
--

LOCK TABLES `khach_hang` WRITE;
/*!40000 ALTER TABLE `khach_hang` DISABLE KEYS */;
/*!40000 ALTER TABLE `khach_hang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kho_hang`
--

DROP TABLE IF EXISTS `kho_hang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kho_hang` (
  `MA_SO` int(11) NOT NULL,
  `TEN` text,
  `DIA_CHI` text,
  `DIEN_THOAI` text,
  PRIMARY KEY (`MA_SO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kho_hang`
--

LOCK TABLES `kho_hang` WRITE;
/*!40000 ALTER TABLE `kho_hang` DISABLE KEYS */;
INSERT INTO `kho_hang` VALUES (1,'Tổng kho','20 Lê Thánh Tôn, Phường Bến Nghé, Quận 1','3823-4796');
/*!40000 ALTER TABLE `kho_hang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `khuyen_mai`
--

DROP TABLE IF EXISTS `khuyen_mai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `khuyen_mai` (
  `MA_SO` int(11) NOT NULL,
  `MAT_HANG` int(11) DEFAULT NULL,
  `THONG_TIN` text,
  `TI_LE_GIAM_GIA` float DEFAULT NULL,
  `TIEN_GIAM_GIA` int(11) DEFAULT NULL,
  PRIMARY KEY (`MA_SO`),
  KEY `MAT_HANG_KHUYEN_MAI_FKEY` (`MAT_HANG`),
  CONSTRAINT `MAT_HANG_KHUYEN_MAI_FKEY` FOREIGN KEY (`MAT_HANG`) REFERENCES `mat_hang` (`MA_SO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `khuyen_mai`
--

LOCK TABLES `khuyen_mai` WRITE;
/*!40000 ALTER TABLE `khuyen_mai` DISABLE KEYS */;
/*!40000 ALTER TABLE `khuyen_mai` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mat_hang`
--

DROP TABLE IF EXISTS `mat_hang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mat_hang` (
  `MA_SO` int(11) NOT NULL,
  `TEN` text,
  `NHA_SAN_XUAT` text,
  `MO_TA` text,
  `GIA` int(11) DEFAULT NULL,
  PRIMARY KEY (`MA_SO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mat_hang`
--

LOCK TABLES `mat_hang` WRITE;
/*!40000 ALTER TABLE `mat_hang` DISABLE KEYS */;
INSERT INTO `mat_hang` VALUES (1,'Miếng dán hạ sốt Koolfever','Koolfever','Koolfever an toàn với trẻ nhỏ vì được cấu tạo bởi lớp Hydrogel (gel chứa nước) và hoàn toàn không chứa thành phần thuốc, nên Kool Fever có tác dụng hạ sốt tự nhiên dựa vào cơ chế vật lý khuyếch tán nhiệt, Kool Fever sử dụng trong trường hợp : Nóng sốt do bệnh lý như: sốt virus, vi khuẩn, viêm đường hô hấp, tiêm chủng... và cả trong trường hợp sốt không do bệnh lý như: nóng do thời tiết, nhiệt độ cao, say nắng...Giảm khó chịu trong trường hợp bị côn trùng cắn, đau đầu. Koolfever được dùng với những lợi ích mang lại như: Phòng ngừa sốt cao co giật giúp giảm ngay cảm giác bứt rứt, khó chịu giảm số lượng thuốc đưa vào cơ thể.',8000),(2,'Bánh quy mặn AFC Animal 100g','AFC','Những chiếc bánh với hình thú đáng yêu cùng hàm lượng dinh dưỡng cao là món quà ý nghĩa mà các bậc phụ huynh có thể tặng cho con mình.Sản phẩm chứa nhiều năng lượng, bổ sung canxi, sắt, và các vitamin cần thiết cho cơ thể, mang đến cho trẻ nguồn năng lượng dồi giàu cho hoạt động vui chơi học tập.',21000),(3,'Kẹo ngậm không đường mentos hương spearmint trà xanh 35g','','Ngành kẹo viên đang có doanh thu ngày càng tăng, điều này chứng tỏ xu hướng sử dụng các dạng kẹo viên, đặc biệt là dạng kẹo có chức năng làm hơi thở thơm mát đang dần phổ biến. Kẹo ngậm không đường mentos hương spearmint trà xanh là sự lựa chọn hợp lý cho các bạn. Tinh chất trà xanh không chỉ giúp hơi thở thơm mát mà còn giúp giảm nhiệt miệng, tạo sự thoải mái cho người dùng. Hương peppermint thơm mát được nhiều người yêu thích, sản phẩm không chứa đường nên phù hợp với cả những người đang giảm cân.',30000),(4,'Trà Ô Long C2 hương chanh 500ml','C2','Sản phẩm trà ô long hương chanh độc đáo và mới lạ với vị trà đậm đà kết hợp cùng hương chanh tươi mát, mang lại cho người uống cảm giác sảng khoái và mát lạnh. sản phẩm thích hợp với nhiều đối tượng khách hàng.',8000),(5,'KitKat trà xanh (12 pcs)','','Những sản phẩm với hương vị trà xanh thơm mát mới lạ đang tạo nên sự thích thú cho các bạn trẻ không chỉ ở Việt Nam. Bắt kịp nhu cầu này, KitKat trà xanh được sản xuất góp phần làm phong phú thêm sự chọn lựa của người dùng. Bạn có thể thưởng thức chiếc bánh xốp giòn thơm phủ bên ngoài lớp socola trà xanh thơm mát, hương vị đọng lại sau khi ăn chắc chằn sẽ làm bạn thích thú.',135000),(6,'Bánh gạo Nhật Ichi 60g','IChi','Được sản xuất từ 100% gạo Nhật, hương vị shouyu mật ong đậm đà mang đến cho bạn những trải nghiệm tuyệt vời như đang thưởng thức chiếc bánh chính thống trên đất nước hoa anh đào. Đậm đà, thơm ngon và khó quên là những gì đọng lại sau khi thường thức.',14000),(7,'Pepsi VIVO Pet 400ml','Pepsi','Sản phẩm pepsi vivo phiên bản đặc biệt có hương vị độc đáo mới từ hương chanh và nước cola, với hương vị sảng khoái và được lấy cảm hứng thiết kế bao bì từ nền văn hóa đầy màu sắc của Brazil , tạo nên sự mới lạ và thích thú nhằm hưởng ứng cho mùa World Cup sắp tới. Sản phẩm đặc biệt thích hợp cho đối tượng giới trẻ.',8000);
/*!40000 ALTER TABLE `mat_hang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nha_cung_cap`
--

DROP TABLE IF EXISTS `nha_cung_cap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nha_cung_cap` (
  `MA_SO` int(11) NOT NULL,
  `TEN` text,
  `DIA_CHI` text,
  `DIEN_THOAI` text,
  PRIMARY KEY (`MA_SO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nha_cung_cap`
--

LOCK TABLES `nha_cung_cap` WRITE;
/*!40000 ALTER TABLE `nha_cung_cap` DISABLE KEYS */;
INSERT INTO `nha_cung_cap` VALUES (1,'Công ty ABC','111 Nguyễn Văn Cừ, p1, q5','8754-1223'),(2,'Công ty CBA','225 Trần Hưng Đạo, Bến Thành, q1','6633-9875');
/*!40000 ALTER TABLE `nha_cung_cap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nhan_vien`
--

DROP TABLE IF EXISTS `nhan_vien`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nhan_vien` (
  `MA_SO` int(11) NOT NULL,
  `HOTEN` text,
  `PHAI` tinyint(1) DEFAULT NULL,
  `NGAY_SINH` date DEFAULT NULL,
  `CMND` text,
  `DIA_CHI` text,
  `DIEN_THOAI` text,
  `CUA_HANG` int(11) DEFAULT NULL,
  PRIMARY KEY (`MA_SO`),
  KEY `CUA_HANG_NHAN_VIEN_FKEY` (`CUA_HANG`),
  CONSTRAINT `CUA_HANG_NHAN_VIEN_FKEY` FOREIGN KEY (`CUA_HANG`) REFERENCES `cua_hang` (`MA_SO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nhan_vien`
--

LOCK TABLES `nhan_vien` WRITE;
/*!40000 ALTER TABLE `nhan_vien` DISABLE KEYS */;
INSERT INTO `nhan_vien` VALUES (1,'Đoàn Minh Triều',0,'0000-00-00','331752214','231C/17 Dương Bá Trạc, p1, q8','0938764679',1),(2,'Trần Anh Thoại',0,'0000-00-00','331723123','P7, quận 5','01234123456',1),(3,'Nguyễn Hữu Trí',0,'0000-00-00','331752110','345a/123 Nguyễn Trãi, q1','0987543210',2),(4,'Hồ Duy Tín',0,'0000-00-00','33179999','12a/5 An Vương Vương, q5','098933399',2),(5,'Nguyễn Hoài An',0,'0000-00-00','123456712','25/3 Lạc Long Quân, Q.10, TP HCM','0976665556',3),(6,'Trần Trà Hương',1,'0000-00-00','223445677','125 Trần Hưng Đạo, Q1, TP HCM','0922333444',3),(7,'Nguyễn Ngọc Ánh',1,'0000-00-00','33111222333','12/21 Võ Văn Ngân, Thủ Đức, TP HCM','091234234',4),(8,'Trương Nam Sơn',0,'0000-00-00','290008778','215 Lý Thường Kiệt, TP Biên Hòa','01223443344',4),(9,'Lý Hoàng Hà',0,'0000-00-00','012987345','22/5 Nguyễn Xí, Q.Bình Thạnh, TP HCM','090123789',5),(10,'Trần Bạch Tuyết',1,'0000-00-00','131113311','127 Hùng Vương, TP Mỹ Tho','091123789',5),(11,'Nguyễn An Trung',0,'0000-00-00','456789012','234 3.2, TP.Biên Hòa','094444555',6),(12,'Trần Trung Hiếu',0,'0000-00-00','267811543','22/11 Lý Thường Kiệt, TP Mỹ Tho','093666999',6),(13,'Trần Hoàng Nam',0,'0000-00-00','100100100','234 Trấn Não, An Phú, TP HCM','097777888',7),(14,'Phạm Nam Thanh',0,'0000-00-00','765890234','221 Hùng Vương, Q.5, TP HCM','01234776655',8),(15,'Đoàn Minh Nguyên',0,'0000-00-00','331752215','231C/17 Dương Bá Trạc, p1, q8','0909001100',9),(16,'Đoàn Thị Ngọc Bích',1,'0000-00-00','234657890','231C/17 Dương Bá Trạc, p1, q8','0908123456',10);
/*!40000 ALTER TABLE `nhan_vien` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phieu_nhap_hang`
--

DROP TABLE IF EXISTS `phieu_nhap_hang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phieu_nhap_hang` (
  `MA_SO` int(11) NOT NULL,
  `MA_NHAN_DANG` text,
  `NGAY_NHAP` date DEFAULT NULL,
  `NHA_CUNG_CAP` int(11) DEFAULT NULL,
  `NHAN_VIEN_NHAP` int(11) DEFAULT NULL,
  `TINH_TRANG` text,
  `NGAY_TAO` date DEFAULT NULL,
  `NGAY_CAP_NHAT` date DEFAULT NULL,
  PRIMARY KEY (`MA_SO`),
  KEY `NHA_CUNG_CAP_PNH_FKEY` (`NHA_CUNG_CAP`),
  KEY `NHAN_VIEN_NHAP_PNH_FKEY` (`NHAN_VIEN_NHAP`),
  CONSTRAINT `NHAN_VIEN_NHAP_PNH_FKEY` FOREIGN KEY (`NHAN_VIEN_NHAP`) REFERENCES `nhan_vien` (`MA_SO`),
  CONSTRAINT `NHA_CUNG_CAP_PNH_FKEY` FOREIGN KEY (`NHA_CUNG_CAP`) REFERENCES `nha_cung_cap` (`MA_SO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phieu_nhap_hang`
--

LOCK TABLES `phieu_nhap_hang` WRITE;
/*!40000 ALTER TABLE `phieu_nhap_hang` DISABLE KEYS */;
/*!40000 ALTER TABLE `phieu_nhap_hang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phieu_xuat_hang`
--

DROP TABLE IF EXISTS `phieu_xuat_hang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phieu_xuat_hang` (
  `MA_SO` int(11) NOT NULL,
  `MA_NHAN_DANG` text,
  `CUA_HANG` int(11) DEFAULT NULL,
  `DIA_CHI` text,
  `NGAY_GIAO` date DEFAULT NULL,
  `NHAN_VIEN_LAP` int(11) DEFAULT NULL,
  `NHAN_VIEN_GIAO_HANG` int(11) DEFAULT NULL,
  `TINH_TRANG` text,
  `NGAY_TAO` date DEFAULT NULL,
  `NGAY_CAP_NHAT` date DEFAULT NULL,
  PRIMARY KEY (`MA_SO`),
  KEY `NHAN_VIEN_LAP_PXH_FKEY` (`NHAN_VIEN_LAP`),
  KEY `NHAN_VIEN_GIAO_HANG_PXH_FKEY` (`NHAN_VIEN_GIAO_HANG`),
  KEY `CUA_HANG_PXH_FKEY` (`CUA_HANG`),
  CONSTRAINT `CUA_HANG_PXH_FKEY` FOREIGN KEY (`CUA_HANG`) REFERENCES `cua_hang` (`MA_SO`),
  CONSTRAINT `NHAN_VIEN_GIAO_HANG_PXH_FKEY` FOREIGN KEY (`NHAN_VIEN_GIAO_HANG`) REFERENCES `nhan_vien` (`MA_SO`),
  CONSTRAINT `NHAN_VIEN_LAP_PXH_FKEY` FOREIGN KEY (`NHAN_VIEN_LAP`) REFERENCES `nhan_vien` (`MA_SO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phieu_xuat_hang`
--

LOCK TABLES `phieu_xuat_hang` WRITE;
/*!40000 ALTER TABLE `phieu_xuat_hang` DISABLE KEYS */;
/*!40000 ALTER TABLE `phieu_xuat_hang` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-09 21:55:30
