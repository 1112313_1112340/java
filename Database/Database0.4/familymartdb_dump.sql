CREATE DATABASE  IF NOT EXISTS `familymartdb` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `familymartdb`;
-- MySQL dump 10.13  Distrib 5.6.13, for Win32 (x86)
--
-- Host: localhost    Database: familymartdb
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `chi_tiet_cua_hang`
--

DROP TABLE IF EXISTS `chi_tiet_cua_hang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chi_tiet_cua_hang` (
  `CUA_HANG` int(11) NOT NULL DEFAULT '0',
  `MAT_HANG` int(11) NOT NULL DEFAULT '0',
  `MIN` int(11) DEFAULT NULL,
  `MAX` int(11) DEFAULT NULL,
  `SO_LUONG` int(11) DEFAULT NULL,
  PRIMARY KEY (`CUA_HANG`,`MAT_HANG`),
  KEY `CHI_TIET_CUA_HANG_MAT_HANG_FKEY` (`MAT_HANG`),
  CONSTRAINT `CHI_TIET_CUA_HANG_CUA_HANG_FKEY` FOREIGN KEY (`CUA_HANG`) REFERENCES `cua_hang` (`MA_SO`),
  CONSTRAINT `CHI_TIET_CUA_HANG_MAT_HANG_FKEY` FOREIGN KEY (`MAT_HANG`) REFERENCES `mat_hang` (`MA_SO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chi_tiet_cua_hang`
--

LOCK TABLES `chi_tiet_cua_hang` WRITE;
/*!40000 ALTER TABLE `chi_tiet_cua_hang` DISABLE KEYS */;
INSERT INTO `chi_tiet_cua_hang` VALUES (1,1,2,100,98),(1,2,2,500,398),(1,3,2,50,50),(1,4,10,100,90),(1,5,2,500,450),(1,6,2,100,0),(1,7,2,200,0),(1,8,2,50,0),(1,9,2,500,0),(1,10,5,500,0),(1,11,2,50,0),(1,12,2,100,0),(1,13,2,100,0),(1,14,2,50,0),(1,15,2,50,0),(1,16,2,50,0),(1,17,5,200,0),(2,1,5,250,0),(2,2,5,500,0),(2,3,2,50,0),(2,4,10,100,0),(2,5,2,300,0),(2,6,2,100,0),(2,7,2,200,0),(2,8,2,50,0),(2,9,2,250,0),(2,10,5,1000,0),(3,2,10,1000,0),(3,7,10,1000,0),(3,10,10,2000,0);
/*!40000 ALTER TABLE `chi_tiet_cua_hang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chi_tiet_hoa_don`
--

DROP TABLE IF EXISTS `chi_tiet_hoa_don`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chi_tiet_hoa_don` (
  `HOA_DON` int(11) NOT NULL DEFAULT '0',
  `MAT_HANG` int(11) NOT NULL DEFAULT '0',
  `KHUYEN_MAI` int(11) DEFAULT NULL,
  `SO_LUONG` int(11) DEFAULT NULL,
  `DON_GIA_GOC` int(11) DEFAULT NULL,
  `GIAM_GIA` int(11) DEFAULT NULL,
  PRIMARY KEY (`HOA_DON`,`MAT_HANG`),
  KEY `MAT_HANG_CTHD_FKEY` (`MAT_HANG`),
  KEY `KHUYEN_MAI_CTHD_FKEY` (`KHUYEN_MAI`),
  CONSTRAINT `HOA_DON_CTHD_FKEY` FOREIGN KEY (`HOA_DON`) REFERENCES `hoa_don` (`MA_SO`),
  CONSTRAINT `KHUYEN_MAI_CTHD_FKEY` FOREIGN KEY (`KHUYEN_MAI`) REFERENCES `khuyen_mai` (`MA_SO`),
  CONSTRAINT `MAT_HANG_CTHD_FKEY` FOREIGN KEY (`MAT_HANG`) REFERENCES `mat_hang` (`MA_SO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chi_tiet_hoa_don`
--

LOCK TABLES `chi_tiet_hoa_don` WRITE;
/*!40000 ALTER TABLE `chi_tiet_hoa_don` DISABLE KEYS */;
INSERT INTO `chi_tiet_hoa_don` VALUES (1,1,1,2,30000,0),(1,2,NULL,2,15000,0);
/*!40000 ALTER TABLE `chi_tiet_hoa_don` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chi_tiet_kho_hang`
--

DROP TABLE IF EXISTS `chi_tiet_kho_hang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chi_tiet_kho_hang` (
  `KHO_HANG` int(11) NOT NULL DEFAULT '0',
  `MAT_HANG` int(11) NOT NULL DEFAULT '0',
  `SO_LUONG` int(11) DEFAULT NULL,
  `MIN` int(11) DEFAULT NULL,
  `MAX` int(11) DEFAULT NULL,
  PRIMARY KEY (`KHO_HANG`,`MAT_HANG`),
  KEY `MAT_HANG_CTKH_FKEY` (`MAT_HANG`),
  CONSTRAINT `KHO_HANG_CTKH_FKEY` FOREIGN KEY (`KHO_HANG`) REFERENCES `kho_hang` (`MA_SO`),
  CONSTRAINT `MAT_HANG_CTKH_FKEY` FOREIGN KEY (`MAT_HANG`) REFERENCES `mat_hang` (`MA_SO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chi_tiet_kho_hang`
--

LOCK TABLES `chi_tiet_kho_hang` WRITE;
/*!40000 ALTER TABLE `chi_tiet_kho_hang` DISABLE KEYS */;
INSERT INTO `chi_tiet_kho_hang` VALUES (1,1,100,5,500),(1,2,1600,10,2000),(1,3,50,2,200),(1,4,110,10,200),(1,5,4550,10,5000),(1,6,200,2,200),(1,7,700,10,2000),(1,8,100,2,100),(1,10,5000,10,5000),(1,11,50,5,150),(1,12,100,2,200),(1,13,100,2,200),(1,14,100,2,100),(1,15,50,2,100),(1,16,50,2,100),(1,17,300,2,1000);
/*!40000 ALTER TABLE `chi_tiet_kho_hang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chi_tiet_phieu_nhap`
--

DROP TABLE IF EXISTS `chi_tiet_phieu_nhap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chi_tiet_phieu_nhap` (
  `PHIEU_NHAP_HANG` int(11) NOT NULL DEFAULT '0',
  `MAT_HANG` int(11) NOT NULL DEFAULT '0',
  `SO_LUONG` int(11) DEFAULT NULL,
  `DON_GIA` int(11) DEFAULT NULL,
  PRIMARY KEY (`PHIEU_NHAP_HANG`,`MAT_HANG`),
  KEY `MAT_HANG_CTPN_FKEY` (`MAT_HANG`),
  CONSTRAINT `MAT_HANG_CTPN_FKEY` FOREIGN KEY (`MAT_HANG`) REFERENCES `mat_hang` (`MA_SO`),
  CONSTRAINT `PHIEU_NHAP_HANG_CTPN_FKEY` FOREIGN KEY (`PHIEU_NHAP_HANG`) REFERENCES `phieu_nhap_hang` (`MA_SO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chi_tiet_phieu_nhap`
--

LOCK TABLES `chi_tiet_phieu_nhap` WRITE;
/*!40000 ALTER TABLE `chi_tiet_phieu_nhap` DISABLE KEYS */;
INSERT INTO `chi_tiet_phieu_nhap` VALUES (1,1,200,27500),(1,2,2000,13500),(1,3,100,100000),(1,4,200,10500),(1,5,5000,20000),(1,6,200,27500),(1,7,500,6500),(1,8,100,129000),(1,10,5000,6500),(2,7,200,6500),(2,11,50,70000),(2,12,100,57500),(2,13,100,57500),(2,14,50,7000),(3,14,50,6500),(3,15,50,90000),(3,16,50,26000),(3,17,100,11000),(4,17,200,10500);
/*!40000 ALTER TABLE `chi_tiet_phieu_nhap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chi_tiet_phieu_xuat`
--

DROP TABLE IF EXISTS `chi_tiet_phieu_xuat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chi_tiet_phieu_xuat` (
  `PHIEU_XUAT_HANG` int(11) NOT NULL DEFAULT '0',
  `MAT_HANG` int(11) NOT NULL DEFAULT '0',
  `SO_LUONG` int(11) DEFAULT NULL,
  `DON_GIA` int(11) DEFAULT NULL,
  PRIMARY KEY (`PHIEU_XUAT_HANG`,`MAT_HANG`),
  KEY `MAT_HANG_CTPX_FKEY` (`MAT_HANG`),
  CONSTRAINT `MAT_HANG_CTPX_FKEY` FOREIGN KEY (`MAT_HANG`) REFERENCES `mat_hang` (`MA_SO`),
  CONSTRAINT `PHIEU_XUAT_HANG_CTPX_FKEY` FOREIGN KEY (`PHIEU_XUAT_HANG`) REFERENCES `phieu_xuat_hang` (`MA_SO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chi_tiet_phieu_xuat`
--

LOCK TABLES `chi_tiet_phieu_xuat` WRITE;
/*!40000 ALTER TABLE `chi_tiet_phieu_xuat` DISABLE KEYS */;
INSERT INTO `chi_tiet_phieu_xuat` VALUES (1,1,100,30000),(1,2,400,15000),(1,3,50,102000),(1,4,90,12000),(1,5,450,21000),(2,1,6,180000),(3,1,6,180000),(4,1,6,180000),(5,1,6,180000),(6,1,6,180000),(7,1,6,180000),(8,1,6,180000),(9,1,6,180000),(10,1,6,180000),(11,1,6,180000),(12,1,6,180000),(13,1,6,180000),(14,1,6,180000),(15,1,6,180000),(16,1,6,180000),(17,1,6,180000),(18,1,6,180000),(19,1,6,180000),(20,1,6,180000),(21,1,6,180000),(22,1,6,180000),(23,1,5,150000),(29,5,11,231000),(30,5,11,231000),(31,5,11,231000),(32,5,11,231000),(33,5,11,231000),(34,5,11,231000);
/*!40000 ALTER TABLE `chi_tiet_phieu_xuat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cua_hang`
--

DROP TABLE IF EXISTS `cua_hang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cua_hang` (
  `MA_SO` int(11) NOT NULL,
  `TEN` text,
  `DIA_CHI` text,
  `DIEN_THOAI` text,
  PRIMARY KEY (`MA_SO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cua_hang`
--

LOCK TABLES `cua_hang` WRITE;
/*!40000 ALTER TABLE `cua_hang` DISABLE KEYS */;
INSERT INTO `cua_hang` VALUES (1,'Cửa hàng Saigon Sky Garden','20 Lê Thánh Tôn, Phường Bến Nghé, Quận 1','3823-4796'),(2,'Cửa hàng ZENPLAZA','Hầm B1, ZENPLAZA, 54-56 Nguyễn Trãi, Phường Bến Thành, Quận 1','3925-5096'),(3,'Cửa hàng 126 Lý Tự Trọng','126 Lý Tự Trọng, Phường Bến Thành, Quận 1','3823-9604'),(4,'Cửa hàng 134 Pasteur','134 Pasteur, Phường Bến Nghé, Quận 1','3827-4396'),(5,'Cửa hàng 58 Pasteur','58 Pasteur, Phường Bến Nghé, Quận 1','3914-7538'),(6,'Cửa hàng Lê Thánh Tôn, chợ Bến Thành','189 Lê Thánh Tôn, Phường Bến Thành, Quận 1','3822-4524'),(7,'Cửa hàng 137 Cô Bắc','137-139 Cô Bắc, Phường Cô Giang, Quận 1','3838-9264'),(8,'Cửa hàng 66C Phó Đức Chính','66C Phó Đức Chính, Phường Nguyễn Thái Bình, Quận 1','3914-6075'),(9,'Cửa hàng 97-99 Nguyễn Hữu Cầu','97-99 Nguyễn Hữu Cầu, Phường Tân Định, Quận 1',' 3820-3950'),(10,'Cửa hàng 234 Đề Thám','234 Đề Thám, Phường Phạm Ngũ Lão, Quận 1','3920-6214');
/*!40000 ALTER TABLE `cua_hang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hoa_don`
--

DROP TABLE IF EXISTS `hoa_don`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hoa_don` (
  `MA_SO` int(11) NOT NULL,
  `MA_NHAN_DANG` text,
  `NGAY_BAN` date DEFAULT NULL,
  `NHAN_VIEN_BAN` int(11) DEFAULT NULL,
  `CUA_HANG` int(11) DEFAULT NULL,
  `KHACH_HANG` int(11) DEFAULT NULL,
  `SO_LUONG` int(11) DEFAULT NULL,
  `THANH_TIEN` int(11) DEFAULT NULL,
  `THUE` int(11) DEFAULT NULL,
  `TONG_CONG` int(11) DEFAULT NULL,
  PRIMARY KEY (`MA_SO`),
  KEY `NHAN_VIEN_BAN_HOADON_FKEY` (`NHAN_VIEN_BAN`),
  KEY `CUA_HANG_HOADON_FKEY` (`CUA_HANG`),
  KEY `KHACH_HANG_HOADON_FKEY` (`KHACH_HANG`),
  CONSTRAINT `CUA_HANG_HOADON_FKEY` FOREIGN KEY (`CUA_HANG`) REFERENCES `cua_hang` (`MA_SO`),
  CONSTRAINT `KHACH_HANG_HOADON_FKEY` FOREIGN KEY (`KHACH_HANG`) REFERENCES `khach_hang` (`MA_SO`),
  CONSTRAINT `NHAN_VIEN_BAN_HOADON_FKEY` FOREIGN KEY (`NHAN_VIEN_BAN`) REFERENCES `nhan_vien` (`MA_SO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hoa_don`
--

LOCK TABLES `hoa_don` WRITE;
/*!40000 ALTER TABLE `hoa_don` DISABLE KEYS */;
INSERT INTO `hoa_don` VALUES (1,'N1','2014-06-15',2,1,NULL,2,90000,0,90000);
/*!40000 ALTER TABLE `hoa_don` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `khach_hang`
--

DROP TABLE IF EXISTS `khach_hang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `khach_hang` (
  `MA_SO` int(11) NOT NULL,
  `HO_TEN` text,
  `DIA_CHI` text,
  `DIEN_THOAI` text,
  `LA_KHACH_HANG_THAN_THIET` tinyint(1) DEFAULT NULL,
  `DIEM_TICH_LUY` int(11) DEFAULT NULL,
  PRIMARY KEY (`MA_SO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `khach_hang`
--

LOCK TABLES `khach_hang` WRITE;
/*!40000 ALTER TABLE `khach_hang` DISABLE KEYS */;
INSERT INTO `khach_hang` VALUES (1,'Nguyễn Thị Bé Nhỏ','P1, Q8','01234192435',0,0),(2,'Trần Văn Ba','P2, Q5','0987121337',0,0);
/*!40000 ALTER TABLE `khach_hang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kho_hang`
--

DROP TABLE IF EXISTS `kho_hang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kho_hang` (
  `MA_SO` int(11) NOT NULL,
  `TEN` text,
  `DIA_CHI` text,
  `DIEN_THOAI` text,
  PRIMARY KEY (`MA_SO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kho_hang`
--

LOCK TABLES `kho_hang` WRITE;
/*!40000 ALTER TABLE `kho_hang` DISABLE KEYS */;
INSERT INTO `kho_hang` VALUES (1,'Tổng kho','20 Lê Thánh Tôn, Phường Bến Nghé, Quận 1','3823-4796');
/*!40000 ALTER TABLE `kho_hang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `khuyen_mai`
--

DROP TABLE IF EXISTS `khuyen_mai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `khuyen_mai` (
  `MA_SO` int(11) NOT NULL,
  `MAT_HANG` int(11) DEFAULT NULL,
  `THONG_TIN` text,
  `TI_LE_GIAM_GIA` float DEFAULT NULL,
  `TIEN_GIAM_GIA` int(11) DEFAULT NULL,
  PRIMARY KEY (`MA_SO`),
  KEY `MAT_HANG_KHUYEN_MAI_FKEY` (`MAT_HANG`),
  CONSTRAINT `MAT_HANG_KHUYEN_MAI_FKEY` FOREIGN KEY (`MAT_HANG`) REFERENCES `mat_hang` (`MA_SO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `khuyen_mai`
--

LOCK TABLES `khuyen_mai` WRITE;
/*!40000 ALTER TABLE `khuyen_mai` DISABLE KEYS */;
INSERT INTO `khuyen_mai` VALUES (1,1,'Mua 2 tặng 1 Lindt Lindor milk chocolate Trio 17g',0,0),(2,3,'Tiết kiệm 5%',0.05,0),(3,4,'Tiết kiệm 2000 đồng.',0,2000);
/*!40000 ALTER TABLE `khuyen_mai` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mat_hang`
--

DROP TABLE IF EXISTS `mat_hang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mat_hang` (
  `MA_SO` int(11) NOT NULL,
  `TEN` text,
  `NHA_SAN_XUAT` text,
  `MO_TA` text,
  `GIA` int(11) DEFAULT NULL,
  PRIMARY KEY (`MA_SO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mat_hang`
--

LOCK TABLES `mat_hang` WRITE;
/*!40000 ALTER TABLE `mat_hang` DISABLE KEYS */;
INSERT INTO `mat_hang` VALUES (1,'Lindt Lindor milk chocolate Trio 37g','Lindt','Đến từ thương hiệu Lindt nổi tiếng, mỗi set gồm 3 viên socola sữa thơm ngon, rất tiện lợi cho người mua. Thiết kế bao bì hình lập thể đẹp mắt tạo nên sức hấp dẫn đặc biệt cho sản phẩm. Giá cả hợp lý giúp các tín đồ socola có thêm sự lựa chọn.',30000),(2,'Bia Sài Gòn Special lon','Bia Saigon','Sản phẩm bia Saigon Special với thành phần 100% từ lúa mạch, được sản xuất trên dây chuyền công nghệ hiện đại tạo nên một hương vị ngon và độc đáo khác hẳn với các sản phẩm bia khác trên thị trường. Sản phẩm thích hợp dành cho người tiêu dùng trẻ trung, năng động, đặc biệt không gây háo nước và nhức đầu sau khi uống.',15000),(3,'Maybeline Son màu color show 308 Cam nổi bật 3.9g','','Với thành phần tinh dầu jojoba & shea butter từ thiên nhiên giúp dưỡng môi, không làm khô môi mang lại cho bạn đôi môi gợi cảm kiêu kỳ. Màu thời trang, môi thật xinh! Sản phẩm với sắc màu trung tính và thời trang phù hợp với các hoạt động hàng ngày đến những sắc màu rực rỡ cho các buổi đi chơi, đi tiệc.',102000),(4,'Miếng dán hạ sốt Koolfever','','Koolfever an toàn với trẻ nhỏ vì được cấu tạo bởi lớp Hydrogel (gel chứa nước) và hoàn toàn không chứa thành phần thuốc, nên Kool Fever có tác dụng hạ sốt tự nhiên dựa vào cơ chế vật lý khuyếch tán nhiệt, Kool Fever sử dụng trong trường hợp : Nóng sốt do bệnh lý như: sốt virus, vi khuẩn, viêm đường hô hấp, tiêm chủng... và cả trong trường hợp sốt không do bệnh lý như: nóng do thời tiết, nhiệt độ cao, say nắng...Giảm khó chịu trong trường hợp bị côn trùng cắn, đau đầu. Koolfever được dùng với những lợi ích mang lại như: Phòng ngừa sốt cao co giật giúp giảm ngay cảm giác bứt rứt, khó chịu giảm số lượng thuốc đưa vào cơ thể.',12000),(5,'Bánh quy mặn AFC Animal 100g','AFC','Những chiếc bánh với hình thú đáng yêu cùng hàm lượng dinh dưỡng cao là món quà ý nghĩa mà các bậc phụ huynh có thể tặng cho con mình.Sản phẩm chứa nhiều năng lượng, bổ sung canxi, sắt, và các vitamin cần thiết cho cơ thể, mang đến cho trẻ nguồn năng lượng dồi giàu cho hoạt động vui chơi học tập.',21000),(6,'Kẹo ngậm không đường mentos hương spearmint trà xanh 35g','','Ngành kẹo viên đang có doanh thu ngày càng tăng, điều này chứng tỏ xu hướng sử dụng các dạng kẹo viên, đặc biệt là dạng kẹo có chức năng làm hơi thở thơm mát đang dần phổ biến. Kẹo ngậm không đường mentos hương spearmint trà xanh là sự lựa chọn hợp lý cho các bạn. Tinh chất trà xanh không chỉ giúp hơi thở thơm mát mà còn giúp giảm nhiệt miệng, tạo sự thoải mái cho người dùng. Hương peppermint thơm mát được nhiều người yêu thích, sản phẩm không chứa đường nên phù hợp với cả những người đang giảm cân.',30000),(7,'Trà Ô Long C2 hương chanh 500ml ','','Sản phẩm trà ô long hương chanh độc đáo và mới lạ với vị trà đậm đà kết hợp cùng hương chanh tươi mát, mang lại cho người uống cảm giác sảng khoái và mát lạnh. sản phẩm thích hợp với nhiều đối tượng khách hàng.',7500),(8,'KitKat trà xanh (12 pcs)','','Những sản phẩm với hương vị trà xanh thơm mát mới lạ đang tạo nên sự thích thú cho các bạn trẻ không chỉ ở Việt Nam. Bắt kịp nhu cầu này, KitKat trà xanh được sản xuất góp phần làm phong phú thêm sự chọn lựa của người dùng. Bạn có thể thưởng thức chiếc bánh xốp giòn thơm phủ bên ngoài lớp socola trà xanh thơm mát, hương vị đọng lại sau khi ăn chắc chằn sẽ làm bạn thích thú.',135000),(9,'Bánh gạo Nhật Ichi 60g','IChi','Được sản xuất từ 100% gạo Nhật, hương vị shouyu mật ong đậm đà mang đến cho bạn những trải nghiệm tuyệt vời như đang thưởng thức chiếc bánh chính thống trên đất nước hoa anh đào. Đậm đà, thơm ngon và khó quên là những gì đọng lại sau khi thường thức.',14000),(10,'Pepsi VIVO Pet 400ml','Pepsi','Sản phẩm pepsi vivo phiên bản đặc biệt có hương vị độc đáo mới từ hương chanh và nước cola, với hương vị sảng khoái và được lấy cảm hứng thiết kế bao bì từ nền văn hóa đầy màu sắc của Brazil , tạo nên sự mới lạ và thích thú nhằm hưởng ứng cho mùa World Cup sắp tới. Sản phẩm đặc biệt thích hợp cho đối tượng giới trẻ.',7000),(11,'Sữa rửa mặt khoáng chất trắng da&tẩy trang Nivea 100g','','Sửa rữa mặt khoáng chất trắng da & tẩy trang Nivea với công thức đột phá kết hợp giữa sữa rửa mặt trắng da & khoáng chất dưới dạng các hạt phân tử khoáng chất mặt nạ bùn , làm sạch bề mặt da vào tận sâu trong lỗ chân lông giúp loại bỏ bụi bẩn, nhờn & các loại cặn trang điểm khó trôi một cách hiệu quả,đồng thời kết hợp với tinh chất dưỡng trắng Pearly White giúp dưỡng trắng da,làm sạch 10 loại cặn trang điểm.',72000),(12,'Gum Xylitol hương apple mint 145g','','Thiết kế hũ gia đình tiện lợi và tiết kiệm hơn, sản phẩm mang đến cho gia đình bạn sự bảo vệ tuyệt đối cho hơi thở thơm mát kéo dài. Sản phẩm gum hoàn toàn không có đường, không gây hại cho sức khỏe người sử dụng. Hương apple mint thơm ngọt, sản phẩm là sự lựa chọn tươi mới góp phần bảo vệ sức khỏe gia đình bạn.',60000),(13,'Gum Xylitol hương bạc hà 145g','','Thiết kế hũ gia đình tiện lợi và tiết kiệm hơn, sản phẩm mang đến cho gia đình bạn sự bảo vệ tuyệt đối cho hơi thở thơm mát kéo dài. Sản phẩm gum hoàn toàn không có đường, không gây hại cho sức khỏe người sử dụng. Hương bạc hà quen thuộc với người Việt Nam, sản phẩm là sự lựa chọn sáng suốt góp phần bảo vệ sức khỏe gia đình bạn',60000),(14,'Kem Merino X Đậu Đỏ Gạo Lức','','Sản phẩm  bổ sung thêm các hạt gạo lức bổ dưỡng bao  phủ bên ngoài cùng với lớp chocolate trắng cao cấp. Bạn sẽ được thưởng thức một hương vị đặc biệt thơm ngon, hoàn toàn mới từ dòng kem Merino.',8000),(15,'AXE Xịt ngăn mùi toàn thân Gold 150ml','','Xịt ngăn mùi toàn thân AXE Dark Temptation Gold được chiết xuất từ mùi hương của cam quýt hòa quyện với vị cay nồng với sự kết hợp ngọt ngào của socola nóng chảy ẩn giấu bên trong là mùi hương nam tính của hổ phách. Đó là sự tương phản của hương thơm ngọt ngào, tươi mát tạo ra một mùi hương độc đáo và tinh tế cho phái mạnh.',90000),(16,'Bàn chải Systema Compact','','Lông bàn chải siêu mảnh, mềm mại 0.02 mm với đường kính chỉ bằng 1/10 so với đầu tròn, có khả năng xâm nhập sâu vào kẽ hở cực hẹp giữa răng và nướu , loại bỏ vi khuẩn giúp viền lợi sạch hơn 9 lần, giúp giảm cao răng, ngăn ngừa sâu răng và ngăn ngừa bệnh viêm lợi.',28000),(17,'Poca Wavy vị Bò Bít Tết Manhattan 54g','','Sự cải tiến về công nghệ mang đến sản phẩm Wavy mới với hương vị Bò bít Tết Manhattan đậm đà hơn, lát khoai tây dày hơn, thơm ngon hơn. Lượng khoai tây trong mỗi gói nhiều hơn, tiết kiệm và dễ dàng chia sẻ cùng bạn bè, người thân. Thích hợp cho mọi đối tượng, nhất là các bạn trẻ.',12000);
/*!40000 ALTER TABLE `mat_hang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nha_cung_cap`
--

DROP TABLE IF EXISTS `nha_cung_cap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nha_cung_cap` (
  `MA_SO` int(11) NOT NULL,
  `TEN` text,
  `DIA_CHI` text,
  `DIEN_THOAI` text,
  PRIMARY KEY (`MA_SO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nha_cung_cap`
--

LOCK TABLES `nha_cung_cap` WRITE;
/*!40000 ALTER TABLE `nha_cung_cap` DISABLE KEYS */;
INSERT INTO `nha_cung_cap` VALUES (1,'Công ty ABC','111 Nguyễn Văn Cừ, p1, q5','8754-1223'),(2,'Công ty CBA','225 Trần Hưng Đạo, Bến Thành, q1','6633-9875');
/*!40000 ALTER TABLE `nha_cung_cap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nhan_vien`
--

DROP TABLE IF EXISTS `nhan_vien`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nhan_vien` (
  `MA_SO` int(11) NOT NULL,
  `HOTEN` text,
  `PHAI` tinyint(1) DEFAULT NULL,
  `NGAY_SINH` date DEFAULT NULL,
  `CMND` text,
  `DIA_CHI` text,
  `DIEN_THOAI` text,
  `CUA_HANG` int(11) DEFAULT NULL,
  PRIMARY KEY (`MA_SO`),
  KEY `CUA_HANG_NHAN_VIEN_FKEY` (`CUA_HANG`),
  CONSTRAINT `CUA_HANG_NHAN_VIEN_FKEY` FOREIGN KEY (`CUA_HANG`) REFERENCES `cua_hang` (`MA_SO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nhan_vien`
--

LOCK TABLES `nhan_vien` WRITE;
/*!40000 ALTER TABLE `nhan_vien` DISABLE KEYS */;
INSERT INTO `nhan_vien` VALUES (1,'Đoàn Minh Triều',0,'0000-00-00','331752214','231C/17 Dương Bá Trạc, p1, q8','0938764679',1),(2,'Trần Anh Thoại',0,'0000-00-00','331723123','P7, quận 5','01234123456',1),(3,'Nguyễn Hữu Trí',0,'0000-00-00','331752110','345a/123 Nguyễn Trãi, q1','0987543210',2),(4,'Hồ Duy Tín',0,'0000-00-00','33179999','12a/5 An Vương Vương, q5','098933399',2),(5,'Nguyễn Hoài An',0,'0000-00-00','123456712','25/3 Lạc Long Quân, Q.10, TP HCM','0976665556',3),(6,'Trần Trà Hương',1,'0000-00-00','223445677','125 Trần Hưng Đạo, Q1, TP HCM','0922333444',3),(7,'Nguyễn Ngọc Ánh',1,'0000-00-00','33111222333','12/21 Võ Văn Ngân, Thủ Đức, TP HCM','091234234',4),(8,'Trương Nam Sơn',0,'0000-00-00','290008778','215 Lý Thường Kiệt, TP Biên Hòa','01223443344',4),(9,'Lý Hoàng Hà',0,'0000-00-00','012987345','22/5 Nguyễn Xí, Q.Bình Thạnh, TP HCM','090123789',5),(10,'Trần Bạch Tuyết',1,'0000-00-00','131113311','127 Hùng Vương, TP Mỹ Tho','091123789',5),(11,'Nguyễn An Trung',0,'0000-00-00','456789012','234 3.2, TP.Biên Hòa','094444555',6),(12,'Trần Trung Hiếu',0,'0000-00-00','267811543','22/11 Lý Thường Kiệt, TP Mỹ Tho','093666999',6),(13,'Trần Hoàng Nam',0,'0000-00-00','100100100','234 Trấn Não, An Phú, TP HCM','097777888',7),(14,'Phạm Nam Thanh',0,'0000-00-00','765890234','221 Hùng Vương, Q.5, TP HCM','01234776655',8),(15,'Đoàn Minh Nguyên',0,'0000-00-00','331752215','231C/17 Dương Bá Trạc, p1, q8','0909001100',9),(16,'Đoàn Thị Ngọc Bích',1,'0000-00-00','234657890','231C/17 Dương Bá Trạc, p1, q8','0908123456',10);
/*!40000 ALTER TABLE `nhan_vien` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phieu_nhap_hang`
--

DROP TABLE IF EXISTS `phieu_nhap_hang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phieu_nhap_hang` (
  `MA_SO` int(11) NOT NULL,
  `MA_NHAN_DANG` text,
  `NGAY_NHAP` date DEFAULT NULL,
  `NHA_CUNG_CAP` int(11) DEFAULT NULL,
  `NHAN_VIEN_NHAP` int(11) DEFAULT NULL,
  `TINH_TRANG` text,
  `NGAY_TAO` date DEFAULT NULL,
  `NGAY_CAP_NHAT` date DEFAULT NULL,
  PRIMARY KEY (`MA_SO`),
  KEY `NHA_CUNG_CAP_PNH_FKEY` (`NHA_CUNG_CAP`),
  KEY `NHAN_VIEN_NHAP_PNH_FKEY` (`NHAN_VIEN_NHAP`),
  CONSTRAINT `NHAN_VIEN_NHAP_PNH_FKEY` FOREIGN KEY (`NHAN_VIEN_NHAP`) REFERENCES `nhan_vien` (`MA_SO`),
  CONSTRAINT `NHA_CUNG_CAP_PNH_FKEY` FOREIGN KEY (`NHA_CUNG_CAP`) REFERENCES `nha_cung_cap` (`MA_SO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phieu_nhap_hang`
--

LOCK TABLES `phieu_nhap_hang` WRITE;
/*!40000 ALTER TABLE `phieu_nhap_hang` DISABLE KEYS */;
INSERT INTO `phieu_nhap_hang` VALUES (1,'N1','2014-06-14',1,1,'New','2014-06-14',NULL),(2,'N2','2014-06-14',1,1,'New','2014-06-14',NULL),(3,'N3','2014-06-14',2,1,'New','2014-06-14',NULL),(4,'N4','2014-06-14',1,1,'New','2014-06-14',NULL);
/*!40000 ALTER TABLE `phieu_nhap_hang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phieu_xuat_hang`
--

DROP TABLE IF EXISTS `phieu_xuat_hang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phieu_xuat_hang` (
  `MA_SO` int(11) NOT NULL,
  `MA_NHAN_DANG` text,
  `CUA_HANG` int(11) DEFAULT NULL,
  `DIA_CHI` text,
  `NGAY_GIAO` date DEFAULT NULL,
  `NHAN_VIEN_LAP` int(11) DEFAULT NULL,
  `NHAN_VIEN_GIAO_HANG` int(11) DEFAULT NULL,
  `TINH_TRANG` text,
  `NGAY_TAO` date DEFAULT NULL,
  `NGAY_CAP_NHAT` date DEFAULT NULL,
  PRIMARY KEY (`MA_SO`),
  KEY `NHAN_VIEN_LAP_PXH_FKEY` (`NHAN_VIEN_LAP`),
  KEY `NHAN_VIEN_GIAO_HANG_PXH_FKEY` (`NHAN_VIEN_GIAO_HANG`),
  KEY `CUA_HANG_PXH_FKEY` (`CUA_HANG`),
  CONSTRAINT `CUA_HANG_PXH_FKEY` FOREIGN KEY (`CUA_HANG`) REFERENCES `cua_hang` (`MA_SO`),
  CONSTRAINT `NHAN_VIEN_GIAO_HANG_PXH_FKEY` FOREIGN KEY (`NHAN_VIEN_GIAO_HANG`) REFERENCES `nhan_vien` (`MA_SO`),
  CONSTRAINT `NHAN_VIEN_LAP_PXH_FKEY` FOREIGN KEY (`NHAN_VIEN_LAP`) REFERENCES `nhan_vien` (`MA_SO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phieu_xuat_hang`
--

LOCK TABLES `phieu_xuat_hang` WRITE;
/*!40000 ALTER TABLE `phieu_xuat_hang` DISABLE KEYS */;
INSERT INTO `phieu_xuat_hang` VALUES (1,'N1',1,NULL,'2014-06-15',1,2,'',NULL,NULL),(2,'N2',1,'20 Lê Thánh Tôn, Phường Bến Nghé, Quận 1',NULL,1,NULL,'Tốt',NULL,NULL),(3,'N3',1,'20 Lê Thánh Tôn, Phường Bến Nghé, Quận 1',NULL,1,NULL,'Tốt',NULL,NULL),(4,'N4',1,'20 Lê Thánh Tôn, Phường Bến Nghé, Quận 1',NULL,1,NULL,'Tốt',NULL,NULL),(5,'N5',1,'20 Lê Thánh Tôn, Phường Bến Nghé, Quận 1',NULL,1,NULL,'Tốt',NULL,NULL),(6,'N6',1,'20 Lê Thánh Tôn, Phường Bến Nghé, Quận 1',NULL,1,NULL,'Tốt',NULL,NULL),(7,'N7',1,'20 Lê Thánh Tôn, Phường Bến Nghé, Quận 1',NULL,1,NULL,'Tốt',NULL,NULL),(8,'N8',1,'20 Lê Thánh Tôn, Phường Bến Nghé, Quận 1',NULL,1,NULL,'Tốt',NULL,NULL),(9,'N9',1,'20 Lê Thánh Tôn, Phường Bến Nghé, Quận 1',NULL,1,NULL,'Tốt',NULL,NULL),(10,'N10',1,'20 Lê Thánh Tôn, Phường Bến Nghé, Quận 1',NULL,1,NULL,'Tốt',NULL,NULL),(11,'N11',1,'20 Lê Thánh Tôn, Phường Bến Nghé, Quận 1',NULL,1,NULL,'Tốt',NULL,NULL),(12,'N12',1,'20 Lê Thánh Tôn, Phường Bến Nghé, Quận 1',NULL,1,NULL,'Tốt',NULL,NULL),(13,'N13',1,'20 Lê Thánh Tôn, Phường Bến Nghé, Quận 1',NULL,1,NULL,'Tốt',NULL,NULL),(14,'N14',1,'20 Lê Thánh Tôn, Phường Bến Nghé, Quận 1',NULL,1,NULL,'Tốt',NULL,NULL),(15,'N15',1,'20 Lê Thánh Tôn, Phường Bến Nghé, Quận 1',NULL,1,NULL,'Tốt',NULL,NULL),(16,'N16',1,'20 Lê Thánh Tôn, Phường Bến Nghé, Quận 1',NULL,1,NULL,'Tốt',NULL,NULL),(17,'N17',1,'20 Lê Thánh Tôn, Phường Bến Nghé, Quận 1',NULL,1,NULL,'Tốt',NULL,NULL),(18,'N18',1,'20 Lê Thánh Tôn, Phường Bến Nghé, Quận 1',NULL,1,NULL,'Tốt',NULL,NULL),(19,'N19',1,'20 Lê Thánh Tôn, Phường Bến Nghé, Quận 1',NULL,1,NULL,'Tốt',NULL,NULL),(20,'N20',1,'20 Lê Thánh Tôn, Phường Bến Nghé, Quận 1',NULL,1,NULL,'Tốt',NULL,NULL),(21,'N21',1,'20 Lê Thánh Tôn, Phường Bến Nghé, Quận 1',NULL,1,NULL,'Tốt',NULL,NULL),(22,'N22',1,'20 Lê Thánh Tôn, Phường Bến Nghé, Quận 1',NULL,1,NULL,'Tốt',NULL,NULL),(23,'N23',1,'20 Lê Thánh Tôn, Phường Bến Nghé, Quận 1',NULL,1,NULL,'Tốt',NULL,NULL),(24,'N24',1,'20 Lê Thánh Tôn, Phường Bến Nghé, Quận 1',NULL,1,NULL,'Tốt',NULL,NULL),(25,'N25',1,'20 Lê Thánh Tôn, Phường Bến Nghé, Quận 1',NULL,1,NULL,'Tốt',NULL,NULL),(26,'N26',1,'20 Lê Thánh Tôn, Phường Bến Nghé, Quận 1',NULL,1,NULL,'Tốt',NULL,NULL),(27,'N27',1,'20 Lê Thánh Tôn, Phường Bến Nghé, Quận 1',NULL,1,NULL,'Tốt',NULL,NULL),(28,'N28',1,'20 Lê Thánh Tôn, Phường Bến Nghé, Quận 1',NULL,1,NULL,'Tốt',NULL,NULL),(29,'N29',1,'20 Lê Thánh Tôn, Phường Bến Nghé, Quận 1',NULL,1,NULL,'Tốt',NULL,NULL),(30,'N30',1,'20 Lê Thánh Tôn, Phường Bến Nghé, Quận 1',NULL,1,NULL,'Tốt',NULL,NULL),(31,'N31',1,'20 Lê Thánh Tôn, Phường Bến Nghé, Quận 1',NULL,1,NULL,'Tốt',NULL,NULL),(32,'N32',1,'20 Lê Thánh Tôn, Phường Bến Nghé, Quận 1',NULL,1,NULL,'Tốt',NULL,NULL),(33,'N33',1,'20 Lê Thánh Tôn, Phường Bến Nghé, Quận 1',NULL,1,NULL,'Tốt',NULL,NULL),(34,'N34',1,'20 Lê Thánh Tôn, Phường Bến Nghé, Quận 1',NULL,1,NULL,'Tốt',NULL,NULL);
/*!40000 ALTER TABLE `phieu_xuat_hang` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-15 10:17:34
